---
title: Awesome!
layout: ok
next: intro.html
---
#### You finished the activity!
Do you know the difference between vaccines and medicine now?
When you press next, you will be brought back to the main page. You can then take a quiz from there.
