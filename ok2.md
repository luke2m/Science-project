---
layout: ok
next: 3.html
title: correct
---
#### Yes!
Antibiotic medicines kill bacteria, which are small living things that can cause sickness such as strep throat. Medicine can also replace what’s missing, block production of other things, and kill germs.
